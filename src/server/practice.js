function restaurantPerCountry(zomatoinfo, countryinfo){
    let result = {}
    let idAndName = {}
    for (let idof of countryinfo){
        idAndName[idof['Country Code']] = idof.Country
        
    }
    for (let code of zomatoinfo){
        let codeId = code['Country Code']
        if (idAndName[codeId]){
            if (!result[idAndName[codeId]]){
                result[idAndName[codeId]] = {}   
            }
            if (!result[idAndName[codeId]][code['Restaurant Name']]){
                result[idAndName[codeId]][code['Restaurant Name']] = 1 
            }
            result[idAndName[codeId]][code['Restaurant Name']] +=1  
        }
    }
    return result 
}
module.exports.restaurantPerCountry = restaurantPerCountry

function restaurantPerCountryPerCity(zomatoinfo, countryinfo){
    let result = {}
    let idAndName = {}
    for (let idof of countryinfo){
        idAndName[idof['Country Code']] = idof.Country
        
    }
    for (let code of zomatoinfo){
        let codeId = code['Country Code']
        if (idAndName[codeId]){
            if(!result[idAndName[codeId]]){
                result[idAndName[codeId]]= {}
            }
            if(!result[idAndName[codeId]][code.City]){
                result[idAndName[codeId]][code.City] = {}
            }
            if (!result[idAndName[codeId]][code.City][code['Restaurant Name']]){
                result[idAndName[codeId]][code.City][code['Restaurant Name']] = 1
            }
            result[idAndName[codeId]][code.City][code['Restaurant Name']] += 1
           
        }
    }
    return result
    
   
}
module.exports.restaurantPerCountryPerCity = restaurantPerCountryPerCity;


function pizzaRestaurants(fooddata){
    let result = {}
    for (let item of fooddata){
        if (item['Cuisines'].includes('Pizza')){
            if(!result[item.City]){
                result[item.City] = {}
            }
            if (!result[item.City][item['Restaurant Name']]){
                result[item.City][item['Restaurant Name']] = 1
            }
            result[item.City][item['Restaurant Name']] += 1
        }
    }
    return result
}
module.exports.pizzaRestaurants = pizzaRestaurants;

function expensiveRestaurantInIndia(restaurant, countryinfo, country){
    let result = {}
    let idAndCountry = {}
    for (let each of countryinfo){
        idAndCountry[each['Country Code']] = each.Country
    }
    for (let item of restaurant){
        let countryid = item['Country Code']
        if (idAndCountry[countryid] == country){
            if (item['Price range'] == 4){
                if(!result[item['Restaurant Name']]){
                    result[item['Restaurant Name']] = 1
                }
                result[item['Restaurant Name']] +=1
            }
        }
    }
    return result
}
module.exports.expensiveRestaurantInIndia = expensiveRestaurantInIndia;

function highestRatingRestaurantInIndia(restaurant, countryinfo, country){
    let result = {}
    let idAndCountry = {}
    for (let each of countryinfo){
        idAndCountry[each['Country Code']] = each.Country
    }
    for (let item of restaurant){
        let countryid = item['Country Code']
        if (idAndCountry[countryid] == country){
            if (item['Rating text'] == 'Excellent'){
                if(!result[item['Restaurant Name']]){
                    result[item['Restaurant Name']] = 1
                }
                result[item['Restaurant Name']] +=1
            }
        }
    }
    return result
}
module.exports.highestRatingRestaurantInIndia = highestRatingRestaurantInIndia;

function restaurantPerCusine(items){
    let result = {};
    for (let each of items){
        let cusines = each.Cuisines.split(', ')
        for (let item of cusines){
            if(!result[item]){
                result[item]= 0;
            }
            result[item] += 1;
        }

    }
    return result;
}
module.exports.restaurantPerCusine = restaurantPerCusine

function restaurantPerCountryHof(zomatoinfo, countryinfo){
    let idAndName = countryinfo.reduce(function(initial,current){
        initial[current['Country Code']] = current.Country
        return initial
    },{})
    let result = zomatoinfo.reduce(function(init, acc){
        let codeId = acc['Country Code']
        if (idAndName[codeId]){
            if (!init[idAndName[codeId]]){
                init[idAndName[codeId]] = {}   
            }
            if (!init[idAndName[codeId]][acc['Restaurant Name']]){
                init[idAndName[codeId]][acc['Restaurant Name']] = 1 
            }
            init[idAndName[codeId]][acc['Restaurant Name']] +=1  
        }
        return init
    },{})
    return result
}
module.exports.restaurantPerCountryHof = restaurantPerCountryHof

function restaurantPerCountryPerCityHof(zomatoinfo, countryinfo){
    let idAndName = countryinfo.reduce(function(initial,current){
        initial[current['Country Code']] = current.Country
        return initial
    },{})
    let result = zomatoinfo.reduce(function(init,acc){
        let codeId = acc['Country Code']
        if (idAndName[codeId]){
            if(!init[idAndName[codeId]]){
                init[idAndName[codeId]]= {}
            }
            if(!init[idAndName[codeId]][acc.City]){
                init[idAndName[codeId]][acc.City] = {}
            }
            if (!init[idAndName[codeId]][acc.City][acc['Restaurant Name']]){
                init[idAndName[codeId]][acc.City][acc['Restaurant Name']] = 1
            }
            init[idAndName[codeId]][acc.City][acc['Restaurant Name']] += 1
           
        }
        return init
    },{})
    return result 
}
module.exports.restaurantPerCountryPerCityHof = restaurantPerCountryPerCityHof;

function pizzaRestaurantsHof(fooddata){
    let result = fooddata.reduce(function(initial,current){
        if (current['Cuisines'].includes('Pizza')){
            if(!initial[current.City]){
                initial[current.City] = {}
            }
            if (!initial[current.City][current['Restaurant Name']]){
                initial[current.City][current['Restaurant Name']] = 1
            }
            initial[current.City][current['Restaurant Name']] += 1
        }
        return initial
    },{})
    return result
}
module.exports.pizzaRestaurantsHof = pizzaRestaurantsHof;

function expensiveRestaurantInIndiaHof(restaurant, countryinfo, country){
    let idAndCountry = countryinfo.reduce(function(initial,current){
        initial[current['Country Code']] = current.Country
        return initial
    },{})
    let result = restaurant.reduce(function(init,acc){
        let countryid = acc['Country Code']
        if (idAndCountry[countryid] == country){
            if (acc['Price range'] == 4){
                if(!init[acc['Restaurant Name']]){
                    init[acc['Restaurant Name']] = 1
                }
                init[acc['Restaurant Name']] +=1
            }
        }
        return init
    },{})
    return result
}
module.exports.expensiveRestaurantInIndiaHof = expensiveRestaurantInIndiaHof;

function highestRatingRestaurantInIndiaHof(restaurant, countryinfo, country){
    let idAndCountry = countryinfo.reduce(function(initial,current){
        initial[current['Country Code']] = current.Country
        return initial
    },{})
    let result = restaurant.reduce(function(init,acc){
        let countryid = acc['Country Code']
        if (idAndCountry[countryid] == country){
            if (acc['Rating text'] == 'Excellent'){
                if(!init[acc['Restaurant Name']]){
                    init[acc['Restaurant Name']] = 1
                }
                init[acc['Restaurant Name']] +=1
            }
        }
        return init
    },{})
    return result
}
module.exports.highestRatingRestaurantInIndiaHof = highestRatingRestaurantInIndiaHof;

function restaurantPerCusineHof(items){
    let result = items.reduce(function(initial, current){
        let cusines = current.Cuisines.split(', ')
        let count = cusines.reduce(function(init,current){
            if(!init[current]){
                init[current] = 0
            }
            init[current] += 1
            return initial;
        },initial);
        return count;
    },{})
    return result;
}
module.exports.restaurantPerCusineHof = restaurantPerCusineHof