const csvtojson = require('csvtojson');
const fs = require('fs');
const path = require("path");

const csvFilePath = path.resolve(__dirname, "../data/zomato.csv")
const csvFilePath1 = path.resolve(__dirname, "../data/Country-Code_2.csv")

const {restaurantPerCountry, restaurantPerCountryPerCity, pizzaRestaurants, expensiveRestaurantInIndia, highestRatingRestaurantInIndia, restaurantPerCusine,restaurantPerCountryHof,restaurantPerCountryPerCityHof, pizzaRestaurantsHof, expensiveRestaurantInIndiaHof, highestRatingRestaurantInIndiaHof, restaurantPerCusineHof}= require("./practice.js")
const practiceproblems = async() => {
    const restaurantData = await csvtojson().fromFile(csvFilePath).then((data)=>{return data});
    const countryData = await csvtojson().fromFile(csvFilePath1).then((data)=>{return data});
    let noOfRestaurantsPerCountry = restaurantPerCountry(restaurantData, countryData)
    let noOfRestaurantsPerCountryPerCity = restaurantPerCountryPerCity(restaurantData,countryData)
    let noOfPizzaRestaurants= pizzaRestaurants(restaurantData)
    let mostexpensiveRestaurantInIndia = expensiveRestaurantInIndia(restaurantData,countryData, 'India')
    let mostHighestRatingRestaurantInIndia = highestRatingRestaurantInIndia(restaurantData,countryData, 'India')
    let cusinesPerRestaurant= restaurantPerCusine(restaurantData);
    let noOfRestaurantsPerCountryHof  = restaurantPerCountryHof(restaurantData, countryData);
    let noOfRestaurantsPerCountryPerCityHof = restaurantPerCountryPerCityHof(restaurantData, countryData)
    let noOfPizzaRestaurantsHof =pizzaRestaurantsHof(restaurantData);
    let mostexpensiveRestaurantInIndiaHof = expensiveRestaurantInIndiaHof(restaurantData, countryData, 'India')
    let mostHighestRatingRestaurantInIndiaHof = highestRatingRestaurantInIndiaHof(restaurantData, countryData, 'India')
    let cusinesPerRestaurantHof = restaurantPerCusineHof(restaurantData)

    
    writeFile(path.resolve(__dirname, "../public/output/noOfRestaurantsPerCountry.json"), JSON.stringify(noOfRestaurantsPerCountry, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/noOfRestaurantsPerCountryPerCity.json"), JSON.stringify(noOfRestaurantsPerCountryPerCity, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/noOfPizzaRestaurants.json"), JSON.stringify(noOfPizzaRestaurants, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/mostexpensiveRestaurantInIndia.json"), JSON.stringify(mostexpensiveRestaurantInIndia, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/mostHighestRatingRestaurantInIndia.json"), JSON.stringify(mostHighestRatingRestaurantInIndia, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/cusinesPerRestaurant.json"), JSON.stringify(cusinesPerRestaurant, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/noOfRestaurantsPerCountryHof.json"), JSON.stringify(noOfRestaurantsPerCountryHof, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/noOfRestaurantsPerCountryPerCityHof.json"), JSON.stringify(noOfRestaurantsPerCountryPerCityHof, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/noOfPizzaRestaurantsHof.json"), JSON.stringify(noOfPizzaRestaurantsHof, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/mostexpensiveRestaurantInIndiaHof.json"), JSON.stringify(mostexpensiveRestaurantInIndiaHof, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/mostHighestRatingRestaurantInIndiaHof.json"), JSON.stringify(mostHighestRatingRestaurantInIndiaHof, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/cusinesPerRestaurantHof.json"), JSON.stringify(cusinesPerRestaurantHof, null, 2));

    
}
practiceproblems()
function writeFile(path, deliverydata) {
    fs.writeFile(path, deliverydata, function(err){
        if (err){
            console.log(err);
        }
    })
}